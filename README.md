# Story PPW

<!-- ![Test and Deploy][actions-badge] -->
![pipeline status][pipeline-badge]
<!-- ![coverage report][coverage-badge] -->

Nama  : Aisha Salsabila <br />
NPM   : 1906399902 <br />
Kelas : PPW-A 
<br />
<br />
<br />

## Lisensi

Sebagian templat Bootstrap CSS yang digunakan oleh repositori ini dibuat oleh [Creative Tim][creative-tim] dan didistribusikan dengan lisensi [MIT License][css-license].

<!-- [actions-badge]: https://github.com/laymonage/django-template-heroku/workflows/Test%20and%20Deploy/badge.svg -->
[pipeline-badge]: https://gitlab.com/aishas/story-ppw/badges/master/pipeline.svg
<!-- [coverage-badge]: https://gitlab.com/aishas/story-ppw/badges/master/coverage.svg -->
[css-license]: Material_Kit_LICENSE.md
[creative-tim]: https://www.creative-tim.com/

