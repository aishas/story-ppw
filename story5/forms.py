from django import forms
from .models import Schedule

class ScheduleForm(forms.Form):
    
    course = forms.CharField(
        label = 'Course', 
        max_length = 100, 
        required = True,
        widget = forms.TextInput(
            attrs ={'class': 'form-control'}),
    )

    lecturer = forms.CharField(
        label = 'Lecturer', 
        max_length = 100, 
        required = True,
        widget = forms.TextInput(
            attrs ={'class': 'form-control'}),
    )

    credit = forms.IntegerField(
        label = 'Credits', 
        required = True,
        widget = forms.NumberInput(
            attrs ={'class': 'form-control'}),
    )

    term = forms.CharField(
        label = 'Term', 
        max_length = 100, 
        required = True,
        widget = forms.TextInput(
            attrs ={'class': 'form-control'}),
    )

    classroom = forms.CharField(
        label = 'Classroom', 
        max_length = 100, 
        required = True,
        widget = forms.TextInput(
            attrs ={'class': 'form-control'}),
    )

    description = forms.CharField(
        label = 'Description', 
        max_length = 150, 
        required = True,
        widget = forms.Textarea(
            attrs ={'class': 'form-control', 'rows': '4'}),
    )


