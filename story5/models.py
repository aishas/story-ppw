from django.db import models

# Create your models here.
# Details of the Schedule (course, lecturer, credit(s), term, desc, room)
class Schedule(models.Model):
    course = models.CharField(max_length=150, null=True)
    lecturer = models.CharField(max_length=150, null=True)
    credit = models.IntegerField(null=True)
    term = models.CharField(max_length=150, null=True)
    description = models.TextField(max_length=350, null=True)
    classroom = models.CharField(max_length=150, null=True)
