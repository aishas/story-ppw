from django.urls import path

from . import views

app_name = 'story5'

urlpatterns = [
    path('list/', views.schedule_list, name='schedule_list'),
    path('add/', views.add_schedule, name='add_schedule'),
    path('list/details/<int:index>', views.course_details, name='course_details'),
]