from django.test import TestCase, Client
from django.apps import apps

from story9.apps import Story9Config

# Create your tests here.
class Unit_Test(TestCase):
    def test_url_login(self):
        response = Client().get("/user/login/")
        self.assertEqual(response.status_code, 200)

    def test_templates_login(self):
        response = Client().get("/user/login/")
        self.assertTemplateUsed(response, 'story9/login.html')

    def test_url_login_post(self):
        response = Client().post("/user/login/")
        self.assertEqual(response.status_code, 200)

    def test_templates_login_post(self):
        response = Client().post("/user/login/")
        self.assertTemplateUsed(response, 'story9/login.html')

    def test_url_register(self):
        response = Client().get("/user/register/")
        self.assertEqual(response.status_code, 200)

    def test_url_register_post(self):
        response = Client().post("/user/register/")
        self.assertEqual(response.status_code, 200)

    def test_url_logout(self):
        response = Client().get("/user/logout/")
        self.assertEqual(response.status_code, 302)

    def test_text_login(self):
        response = Client().get("/user/login/")
        html_response = response.content.decode('utf8')
        self.assertIn("Please login first", html_response)
        self.assertIn("Username", html_response)
        self.assertIn("Password", html_response)

    def test_url_user(self):
        response = Client().get("/user/")
        self.assertEqual(response.status_code, 302)
    

class TestApp(TestCase):
	def test_apps(self):
		self.assertEqual(Story9Config.name, 'story9')
		self.assertEqual(apps.get_app_config('story9').name, 'story9')
