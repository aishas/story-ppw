from django.shortcuts import render, redirect # redirect for login register
from django.http import HttpResponse
from django.forms import inlineformset_factory
from django.contrib.auth.forms import UserCreationForm # for register n login form

from django.contrib.auth import authenticate, login, logout # for login

from django.contrib import messages # added for login register

from django.contrib.auth.models import Group

from .forms import CreateUserForm

from django.contrib.auth.decorators import login_required

# Create your views here.


def registerPage(request):
    if request.user.is_authenticated:
        return redirect('story9:index')
    else:
        form = CreateUserForm()

        if request.method == 'POST':
            form = CreateUserForm(request.POST)
            if form.is_valid():
                user = form.save()
                username = form.cleaned_data.get('username')

                messages.success(request, 'Account was created for ' + username)

                return redirect('story9:login')

        context = {'form':form}
        return render(request, 'story9/register.html', context)


def loginPage(request):
    if request.user.is_authenticated:
        return redirect('story9:index')
    else:
        if request.method == 'POST':
            username = request.POST.get('username')
            password = request.POST.get('password')

            user = authenticate(request, username=username, password=password)

            if user is not None:
                login(request, user)
                return redirect('story9:index')
            else:
                messages.info(request, 'Username OR password is incorrect')

        context = {}
        return render(request, 'story9/login.html', context)

def logoutUser(request):
    logout(request)
    return redirect('story9:login')

@login_required(login_url='/user/login')
def index(request):
	return render(request, 'story9/index.html')

