from django.urls import path
from . import views

app_name = 'story9'

urlpatterns = [
    path('', views.index, name='index'),
    path('register/', views.registerPage, name="register"),
	path('login/', views.loginPage, name="login"),  
	path('logout/', views.logoutUser, name="logout"),
    
]