from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
import requests
import json

# Create your views here.

def books(request):
    return render(request, 'story8/books.html')

def search_book(request):
    arg = request.GET['q']
    url = 'https://www.googleapis.com/books/v1/volumes?q=' + arg
    result = requests.get(url)
    data = json.loads(result.content)
    return JsonResponse(data, safe=False)

