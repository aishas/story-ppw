from django.urls import path
from . import views

app_name = 'story8'

urlpatterns = [
    path('', views.books, name='books'),
    path('data/', views.search_book, name='search_book'),
]
