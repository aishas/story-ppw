from django.apps import apps
from django.test import TestCase, Client, LiveServerTestCase
from django.http import HttpRequest

from .apps import Story7Config
from .views import accordion

import os
import time


# Create your tests here.
class TestApp(TestCase):
	def test_apps(self):
		self.assertEqual(Story7Config.name, 'story7')
		self.assertEqual(apps.get_app_config('story7').name, 'story7')


class UnitTest(TestCase):
    def test_landing_page_url_is_exist(self):
        response = Client().get('/accordion/')
        self.assertEqual(response.status_code, 200)

    def test_landing_page_html(self):
        response = Client().get('/accordion/')
        self.assertTemplateUsed(response, 'story7/accordion.html')