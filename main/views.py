from django.shortcuts import render


def home(request):
    return render(request, 'main/home.html')


def work(request):
    return render(request, 'main/work.html')
